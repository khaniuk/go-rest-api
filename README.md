# Docker + Go + PostgreSQL

## Instalar dependencias
```
go get github.com/jinzhu/gorm
go get golang.org/x/crypto/bcrypt
go get github.com/dgrijalva/jwt-go
go get github.com/gorilla/mux
go get github.com/jinzhu/gorm/dialects/postgres //If using postgres
go get github.com/jinzhu/gorm/dialects/mysql //If using mysql
go get github.com/jinzhu/gorm/dialects/sqlite //If using sqlite
```

## Obtener la imagen del Go desde [Docker Hub](https://hub.docker.com/_/golang)

Cuando no se especifica el tag lo trae la última versión.
```
docker pull golang
```

También se puede trabajar con imagenes livianas (alpine)
```
docker pull golang:1.14-alpine3.11
```

## Crear el contenedor temporal con volumen
```
docker run --rm -v /home/ligth/test/got/:/var/www/got -it golang:1.14-alpine3.11
```

Parámetros:
* **--rm** Indica que será temporal, hasta que se salga del contendor.
* **-v** Indica el volumen **/ruta/origen/local : /ruta/destino/contenedor** comunicación en ambas vías.
* **-it** Indica que será interactivo.
* **--name tag** Indica que tag o nombre le asignará al contenedor (opcional).


Al crear un contenedor, se genera un identificador único que puede ser utilizado para ejecutar las siguientes instrucciones:

```
docker start 8514343d79dc
```

```
docker stop 8514343d79dc
```

```
docker inspect 8514343d79dc
```

```
docker attach 8514343d79dc
```

```
docker rm 8514343d79dc
```

```
docker kill 8514343d79dc
```

## Ejecutar el proyecto GO

```
go run main.go
```

## PostgreSQL
## Obtener la imagen del PostgreSQL desde [Docker Hub](https://hub.docker.com/_/postgres)

Cuando no se especifica el tag lo trae la última versión.
```
docker pull postgres
```

También se puede trabajar con imagenes livianas (alpine)
```
docker pull postgres:alpine
```

## Crear el contenedor
```
docker run --name dbgo -e POSTGRES_PASSWORD=postgres -d postgres:latest
```

Parámetros:
* **--name tag** Indica que tag o nombre le asignará al contenedor (opcional).
* **-e** Indica que se asignará la contraseña al usuario **postgres**.
* **-d** Indica que se ejecutará en segundo plano.

Ingresar al bash del contenedor **dbgo**
```
docker exec -it dbgo bash
```

Ingresar al DB de PostgreSQL
```
su postgres
```

Ingresar al PSQL
```
psql
```
```
psql DBNAME USERNAME
```

Instrucciones PSQL
* To view help for psql commands, type **\\?**.
* To view help for SQL commands, type **\h**.
* To view information about the current database connection, type **\conninfo**.
* To list the database's tables and their respective owners, type **\dt**.
* To list all of the tables, views, and sequences in the database, type **\z**.
* To exit the psql program, type **\q**.