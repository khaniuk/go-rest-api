package controllers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"
	"strconv"

	"github.com/gorilla/mux"
	
	"../config"
	"../models"
	"../utils"
)

// CreateUser add new user
func CreateUser(w http.ResponseWriter, r *http.Request) {
	db := config.GetConnection()
	defer db.Close()

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.ERROR(w, http.StatusUnprocessableEntity, err)
	}
	user := models.User{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		utils.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	user.Prepare()
	err = user.Validate("")
	if err != nil {
		utils.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	
	err = db.Debug().Create(&user).Error
	if err != nil {
		formattedError := utils.FormatError(err.Error())
		utils.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	utils.JSON(w, http.StatusCreated, user)
}

// GetUsers get all user registers
func GetUsers(w http.ResponseWriter, r *http.Request) {
	db := config.GetConnection()
	defer db.Close()

	var err error
	users := []models.User{}

	err = db.Debug().Model(&models.User{}).Limit(100).Find(&users).Error
	if err != nil {
		utils.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	utils.JSON(w, http.StatusOK, users)
}

// GetUserByID get user data
func GetUserByID(w http.ResponseWriter, r *http.Request) {
	db := config.GetConnection()
	defer db.Close()

	vars := mux.Vars(r)
	uid, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		utils.ERROR(w, http.StatusBadRequest, err)
		return
	}

	user := models.User{}
	err = db.Debug().Model(&models.User{}).Where("id = ?", uint32(uid)).Take(&user).Error
	if err != nil {
		utils.ERROR(w, http.StatusBadRequest, err)
		return
	}
	
	utils.JSON(w, http.StatusOK, user)
}

// UpdateUser modify user data
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	db := config.GetConnection()
	defer db.Close()

	vars := mux.Vars(r)
	uid, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		utils.ERROR(w, http.StatusBadRequest, err)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		utils.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	user := models.User{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		utils.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	user.Prepare()
	err = user.Validate("update")
	if err != nil {
		utils.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	
	err = user.BeforeSave()
	if err != nil {
		log.Fatal(err)
	}

	db = db.Debug().Model(&models.User{}).Where("id = ?", uint32(uid)).Take(&models.User{}).UpdateColumns(
		map[string]interface{}{
			"nombre": user.Nombre,
			"username": user.Username,
			"password": user.Password,
			"updateAt": time.Now(),
		},
	)
	if db.Error != nil {
		utils.ERROR(w, http.StatusInternalServerError, db.Error)
		return
	}
	// This is the display the updated user
	err = db.Debug().Model(&models.User{}).Where("id = ?", uint32(uid)).Take(&user).Error
	if err != nil {
		formattedError := utils.FormatError(err.Error())
		utils.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	utils.JSON(w, http.StatusOK, user)
}

// DeleteUser delete user data permanently
func DeleteUser(w http.ResponseWriter, r *http.Request) {
	db := config.GetConnection()
	defer db.Close()

	vars := mux.Vars(r)
	user := models.User{}

	uid, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		utils.ERROR(w, http.StatusBadRequest, err)
		return
	}

	db = db.Debug().Model(&models.User{}).Where("id = ?", uint32(uid)).Take(&user).Unscoped().Delete(&user)
	if db.Error != nil {
		utils.ERROR(w, http.StatusInternalServerError, db.Error)
	}

	log.Println(db.RowsAffected)
	utils.JSON(w, http.StatusNoContent, "")
}
