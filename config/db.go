package config

import (
	"encoding/json"
	"log"
	"os"

	"fmt"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	// _ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/jinzhu/gorm"
)

type db struct {
	Driver string
	Host string
	Port string
	User string
	Password string
	Dbname string
}

func getConfig() db {
	var config_db db
	file, err := os.Open("./config/config.json")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	err = json.NewDecoder(file).Decode(&config_db)
	if err != nil {
		log.Fatal(err)
	}

	return config_db
}

// GetConnection obtiene una conexión a la bd
func GetConnection() *gorm.DB {
	var err error
	var db *gorm.DB

	config_db := getConfig()

	if config_db.Driver == "mysql" {
		conn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", config_db.User, config_db.Password, config_db.Host, config_db.Port, config_db.Dbname)
		db, err = gorm.Open(config_db.Driver, conn)
		if err != nil {
			fmt.Printf("Cannot connect to %s database", config_db.Driver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database", config_db.Driver)
		}
	}

	if config_db.Driver == "postgres" {
		conn := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", config_db.Host, config_db.Port, config_db.User, config_db.Dbname, config_db.Password)
		db, err = gorm.Open(config_db.Driver, conn)
		if err != nil {
			fmt.Printf("Cannot connect to %s database", config_db.Driver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database", config_db.Driver)
		}
	}

	if config_db.Driver == "sqlite3" {
		// conn := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", config_db.Host, config_db.Port, config_db.User, config_db.Dbname, config_db.Password)
		db, err = gorm.Open(config_db.Driver, config_db.Dbname)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", config_db.Driver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database\n", config_db.Driver)
		}
		db.Exec("PRAGMA foreign_keys = ON")
	}

	return db
}