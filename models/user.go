package models

import (
	"errors"
	"html"
	"strings"
	// "time"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// User model
type User struct {
	gorm.Model
	// ID        uint32    `gorm:"primary_key;auto_increment" json:"id"`
	Nombre  	string    `gorm:"size:150;not null" json:"nombre"`
	Telefono	string    `gorm:"size:30;not null" json:"telefono"`
	RolId     int    `gorm:"not null" json:"rol_id"`
	Username	string    `gorm:"size:20;not null;unique" json:"username"`
	Password	string    `gorm:"size:100;not null;" json:"password"`
	Estado		bool    `gorm:"not null;" json:"estado`
	// CreatedAt	time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"createdAt"`
	// UpdatedAt	time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updatedAt"`
}

func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

func VerifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func (u *User) BeforeSave() error {
	hashedPassword, err := Hash(u.Password)
	if err != nil {
		return err
	}
	u.Password = string(hashedPassword)
	return nil
}

func (u *User) Prepare() {
	u.ID = 0
	u.Nombre = html.EscapeString(strings.TrimSpace(u.Nombre))
	u.Telefono = html.EscapeString(strings.TrimSpace(u.Telefono))
	u.RolId = u.RolId
	u.Username = html.EscapeString(strings.TrimSpace(u.Username))
	u.Password = html.EscapeString(strings.TrimSpace(u.Password))
	u.Estado = u.Estado
	// u.CreatedAt = time.Now()
	// u.UpdatedAt = time.Now()
}

func (u *User) Validate(action string) error {
	switch strings.ToLower(action) {
	case "update":
		if u.Nombre == "" {
			return errors.New("Required Nombre")
		}
		if u.Password == "" {
			return errors.New("Required Password")
		}
		if u.Username == "" {
			return errors.New("Required Username")
		}
		return nil

	case "login":
		if u.Password == "" {
			return errors.New("Required Password")
		}
		if u.Username == "" {
			return errors.New("Required Username")
		}
		return nil

	default:
		if u.Nombre == "" {
			return errors.New("Required Nombre")
		}
		if u.Password == "" {
			return errors.New("Required Password")
		}
		if u.Username == "" {
			return errors.New("Required Username")
		}
		return nil
	}
}