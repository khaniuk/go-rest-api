package migrations

import (
	"../config"
	"../models"
)

// Migrate create tables in bd
func RunMigrate() {
	db := config.GetConnection()
	defer db.Close()

	db.CreateTable(&models.User{})
	// db.CreateTable(&models.Rol{})
}