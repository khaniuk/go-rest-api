package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"./utils"
	"./migrations"
	"./seeders"
	"./routes"
)

func main() {
	var migrate string
	var seeder string

	flag.StringVar(&migrate, "migrate", "no", "Migration to BD")
	flag.StringVar(&seeder, "seeder", "no", "Seeder to BD")
	flag.IntVar(&utils.Port, "port", 8080, "Server port")
	flag.Parse()
	if migrate == "yes" {
		log.Println("Start migration...")
		migrations.RunMigrate()
		log.Println("End of migration.")
	}
	if seeder == "yes" {
		log.Println("Start seeder...")
		seeders.RunSeeder()
		log.Println("End of seeder.")
	}

	// Initialize routes
	router := routes.InitRoutes()

	// Start the server
	fmt.Println("Listening to port 8080")
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", utils.Port), router))
}