package seeders

import (
	"log"
	// "time"
	
	"../config"
	"../models"
)
var users = []models.User{
	models.User{
		Nombre: "Administrator",
		Telefono: "23456789",
		RolId: 1,
		Username: "admin",
		Password: "admin",
		Estado: true,
		// CreatedAt: time.Now(),
    // UpdatedAt: time.Now(),
	},
	models.User{
		Nombre: "Operator",
		Telefono: "76543214",
		RolId: 2,
		Username: "operator",
		Password: "operator",
		Estado: true,
		// CreatedAt: time.Now(),
    // UpdatedAt: time.Now(),
	},
}

func RunSeeder() {
	db := config.GetConnection()
	defer db.Close()

	for i, _ := range users {
		err := db.Debug().Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			log.Fatalf("cannot seed users table: %v", err)
		}
	}
}
