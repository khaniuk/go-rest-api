package utils

import (
	"errors"
	"strings"
)

func FormatError(err string) error {

	if strings.Contains(err, "nombre") {
		return errors.New("Nombre Already Taken")
	}

	if strings.Contains(err, "username") {
		return errors.New("Username Already Taken")
	}

	return errors.New("Incorrect Details")
}
